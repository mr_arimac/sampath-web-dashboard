import { lazy } from 'react'

const SectionRoutes = [
    {
        path: '/sections/whats_new',
        exact: true,
        appLayout: true,
        component: lazy(() => import('../../views/pages/WhatsNew'))
    },
    {
        path: '/sections/card_promotions',
        exact: true,
        appLayout: true,
        component: lazy(() => import('../../views/pages/CardPromotions'))
    },
    {
        path: '/sections/news_and_media',
        exact: true,
        appLayout: true,
        component: lazy(() => import('../../views/pages/NewsAndMedia'))
    },
    {
        path: '/sections/branches',
        exact: true,
        appLayout: true,
        component: lazy(() => import('../../views/pages/Branches'))
    }
]

export default SectionRoutes
