import { lazy } from 'react'

const PageRoutes = [
    {
        path: '/login',
        component: lazy(() => import('../../views/pages/Login')),
        layout: 'BlankLayout'
    },
    {
        path: '/pages/home',
        exact: true,
        appLayout: true,
        component: lazy(() => import('../../views/pages/Home'))
    }
]

export default PageRoutes
