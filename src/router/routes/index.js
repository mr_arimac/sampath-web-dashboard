// ** Routes Imports
import PagesRoutes from './Pages'
import SectionRoutes from './Sections'

// ** Document title
const TemplateTitle = '%s - Vuexy React Admin Template'

// ** Default Route
const DefaultRoute = '/pages/home'

// ** Merge Routes
const Routes = [...PagesRoutes, ...SectionRoutes]

export { DefaultRoute, TemplateTitle, Routes }
