import axios from "axios"
// import store from "../../store models/store_model";

const connectivity = require("connectivity")

const baseUrl = process.env.REACT_APP_API_BASE_URL

// const showAlertDialog = store.dispatch.message.showMessage;

const headerTypes = {
  ApplicationJson: "ApplicationJson",
  Authorized: "Authorized"
}

const performRequest = async (
  givenMethod,
  givenUrl,
  givenData,
  headerType,
  needLoading
) => {
//   const setLoading = store.dispatch.loader.showLoader;
  const token = localStorage.getItem('accessToken')
  console.log(token)
  const isConnected = connectivity()
  const headers = {
    applicationJson: {
      "Content-Type": "application/json"
    },
    authorized: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  }

  let header
  switch (headerType) {
    case headerTypes.ApplicationJson:
      header = headers.applicationJson
      break
    case headerTypes.Authorized:
      header = headers.authorized
      break

    default:
      header = headers.applicationJson
      break
  }

  console.log(header)
  if (isConnected) {
    // if (needLoading) {
    //   setLoading(true);
    // }

    return await axios({
      baseURL: baseUrl,
      url: givenUrl,
      method: givenMethod,
      data: givenData,
      headers: header
    })
      .then((response) => {
        // if (needLoading) {
        //   setLoading(false);
        // }
        console.log(response.data)
        if (response.status === 200) {
          return {
            success: true,
            data: response.data,
            message: response.statusText,
            status: response.status
          }
        }
        // showAlertDialog({
        //   head: response.data.title,
        //   body: response.data.message,
        // });
        // alert(response.statusText);
        return {
          success: false,
          data: response.data,
          message: response.statusText,
          status: response.status
        }
      })
      .catch((err) => {
        // setLoading(false);
        console.log(err.response)
        if (err.response && err.response.data.title) {
          console.log(err.response.data)
        //   showAlertDialog({
        //     head: err.response.data.title,
        //     body: err.response.data.message,
        //   });
        } else {
        //   showAlertDialog({ head: "Something went wrong!", body: err.message });
        }

        // alert(err);
        return {
          success: false,
          data: err.response ? err.response.data : null,
          message: err,
          status: err.response ? err.response.status : 500
        }
      })
  } else {
    // showAlertDialog({
    //   head: "No Internet Connection",
    //   body: "Please connect to internet and try again!",
    // });
    // return
  }
}


export const loginCall = async (email, pass) => {
    return await performRequest(
      "POST",
      "/auth/local",
      {
        identifier: email,
        password: pass
      },
      headerTypes.ApplicationJson,
      true
    )
  }