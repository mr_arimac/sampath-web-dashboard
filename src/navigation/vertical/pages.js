import { Circle, Package } from 'react-feather'
export default [
    {
        id: 'pages',
        title: 'Pages',
        icon: <Package size={20} />,
        children: [
            {
                id: 'home',
                title: 'Home',
                icon: <Circle size={12} />,
                navLink: '/pages/home'
            }
        ]
    }
]
