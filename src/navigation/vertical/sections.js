import { Circle, Menu } from 'react-feather'
export default [
    {
        id: 'sections',
        title: 'Sections',
        icon: <Menu size={20} />,
        children: [
            {
                id: 'whats_new',
                title: 'What\'s New',
                icon: <Circle size={12} />,
                navLink: '/sections/whats_new'
            },
            {
                id: 'card_promotions',
                title: 'Card Promotions',
                icon: <Circle size={12} />,
                navLink: '/sections/card_promotions'
            },
            {
                id: 'news_and_media',
                title: 'News & Media',
                icon: <Circle size={12} />,
                navLink: '/sections/news_and_media'
            },
            {
                id: 'branches',
                title: 'Branches',
                icon: <Circle size={12} />,
                navLink: '/sections/branches'
            }
        ]
    }
]