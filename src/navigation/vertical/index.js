// ** Navigation sections imports
import pages from './pages'
import sections from './sections'

// ** Merge & Export
export default [...pages, ...sections]
